## requirements.yml defines the sources of roles from git

### install roles with:
 ``` bash 
    ansible-galaxy install -r requirements.yml --force
 ```
 ### then run the playbook